<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/home',['as'=>'dashboard','uses'=>'HomeController@index']);
Route::get('/typography',['as'=>'typography','uses'=>'HomeController@typography']);
Route::get('/helper',['as'=>'helper','uses'=>'HomeController@helper']);
Route::get('/widget',['as'=>'widget','uses'=>'HomeController@widget']);
Route::get('/table',['as'=>'table','uses'=>'HomeController@table']);
Route::get('/media',['as'=>'media','uses'=>'HomeController@media']);
Route::get('/chart',['as'=>'chart','uses'=>'HomeController@chart']);
Route::get('/form',['as'=>'form','uses'=>'HomeController@form']);
 
Route::group(['middleware'=>['auth']],function(){
        // Route::get('/home',function(){
        //         return view('index');
        // });
        Route::group(['prefix'=>'learn-vue','namespace'=>'Dashboard'],function(){
              Route::get('/',['uses'=>'LearnVueController@index']);
        });
        
        Route::get('/home',['as'=>'dashboard','uses'=>'HomeController@index']);
        Route::group(['middleware' => ['role:admin']], function () {
                Route::resource('permissions', 'Admin\PermissionsController');     
                Route::resource('roles', 'Admin\RolesController');     
                Route::resource('users', 'Admin\UsersController'); 
                Route::resource('add-items', 'Admin\AddItemController');  
            });
        
        Route::resource('profile','Users\ProfileController');
});

Route::get('/',['as'=>'transaksi.index.view','uses'=>'Dashboard\TransaksiController@indexView']);
Route::get('/transaksi-data-api',['uses'=>'Dashboard\TransaksiController@index'])
        ->name('transaksi.index.api');
Route::get('/transaksi-product-data-api',['uses'=>'Dashboard\TransaksiController@allProduct'])
        ->name('transaksi.product.index.api');



