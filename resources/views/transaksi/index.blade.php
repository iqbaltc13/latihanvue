<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>List Transaksi</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Bootstrap Core Css -->
    @section('css')
        {{ Html::style('bsbmd/plugins/bootstrap/css/bootstrap.css') }}
       
        {{ Html::style('bsbmd/css/style.css') }}
        {{ Html::style('bsbmd/css/themes/all-themes.css') }}

         <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    @show

    @yield('extra-css')
</head>

<body class="theme-red">
   
    <div class="overlay"></div>
   

   

        <div class="container-fluid">
            <div class="block-header">
                
            </div>

            <!-- 'permanent_address', 'current_address', 'country', 'state', 'avatar', 'city', 'pincode', 'mobile', 'alternate_mobile', 'secondary_email', 'aadhar_no', 'user_id' -->

            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue-grey">
                            <h2>
                               List Transaksi
                            </h2>
                            
                           
                        </div>
                        <div class="body">
                               
                            <div class="table-responsive" style="margin-top: 10px;">
                                <table id="transaksi" class="table table-striped table-hover js-basic-example dataTable"  data-display-length='-1'>
                                    <thead>
                                        <tr>
                                           
                                            <th>No</th>
                                            
                                            <th>Tanggal Beli</th>
                                            <th>Pembeli</th>
                                            <th>Penjual</th>
                                            <th>Nama Produk</th>
                                            <th>Quantity</th>
                                            <th>Harga Satuan</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Vertical Layout -->
           
        </div>


        
        


    @section('script')
        {{Html::script('bsbmd/plugins/jquery/jquery.min.js')}}
        {{Html::script('bsbmd/plugins/bootstrap/js/bootstrap.js')}}
        {{Html::script('bsbmd/plugins/bootstrap-select/js/bootstrap-select.js')}}
        {{Html::script('bsbmd/plugins/jquery-slimscroll/jquery.slimscroll.js')}}
        {{Html::script('bsbmd/plugins/node-waves/waves.js')}}
        {{Html::script('app/transaksi/index.js')}}

    @show    
    @yield('extra-script')
    @section('script-bottom')
        {{Html::script('bsbmd/js/admin.js')}}
        {{Html::script('bsbmd/js/demo.js')}}
    @show
</body>

</html>
