@extends('app')

@section('title')
	Belajar Vue
@endsection

@section('extra-css')
<!-- Colorpicker Css -->
    {{ Html::style('bsbmd/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}

    <!-- Dropzone Css -->
    {{ Html::style('bsbmd/plugins/dropzone/dropzone.css') }}

    <!-- Multi Select Css -->
    {{ Html::style('bsbmd/plugins/multi-select/css/multi-select.css') }}

    <!-- Bootstrap Spinner Css -->
    {{ Html::style('bsbmd/plugins/jquery-spinner/css/bootstrap-spinner.css') }}

    <!-- Bootstrap Tagsinput Css -->
    {{ Html::style('bsbmd/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}

    <!-- Bootstrap Select Css -->
    {{ Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css') }}

    <!-- noUISlider Css -->
    {{ Html::style('bsbmd/plugins/nouislider/nouislider.min.css') }}
	
@endsection

@section('content')
        <div class="container-fluid">
            <div class="block-header">
               
            </div>

            <!-- 'permanent_address', 'current_address', 'country', 'state', 'avatar', 'city', 'pincode', 'mobile', 'alternate_mobile', 'secondary_email', 'aadhar_no', 'user_id' -->

            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue-grey">
                            <h2>
                                Belajar Vue
                            </h2>
                            
                            @php
                             $i=0;   
                            @endphp
                        </div>
                        <div class="body">
                            <div id="learnvue">
                                    <a href="javascript:void(0)" @click="showHideMethod" class="btn bg-teal btn waves-effect show-hide"><i class="material-icons">add</i><span>Button Click 1</span></a>
                                
                                    <a href="javascript:void(0)" v-if="seen"  v-on:click="seen = !seen" class="btn bg-teal btn waves-effect"><i class="material-icons">add</i><span>Button Click 2</span></a>
                                
                                <p v-text="pesan2"></p>
                                <p v-html="pesan3"></p>
                            <p v-if="bool1==true">True!!</p>
                            <p v-if="bool2=='false'">False!!</p>
                            <p>@{{ pesan }}</p> 

                            <div class="table-responsive" style="margin-top: 10px;">
                                    <table id="transaksi" class="table table-striped table-hover js-basic-example dataTable"  data-display-length='-1'>
                                        <thead>
                                            <tr>
                                               
                                                <th>No</th>
                                                
                                                <th>Tanggal Beli</th>
                                                <th>Pembeli</th>
                                                <th>Penjual</th>
                                                <th>Nama Produk</th>
                                                <th>Quantity</th>
                                                <th>Harga Satuan</th>
                                                <th>Subtotal</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody  v-for="(d,index) in ds">
                                            <tr>
                                                
                                                
                                                {{-- <td>@{{indexCounter}}</td> --}}
                                                <td>@{{indexes(index)}}</td>
                                                <td>@{{d.tanggal_beli}}</td>
                                                <td>@{{d.pembeli}}</td>
                                                <td>@{{d.penjual}}</td>
                                                <td>@{{d.product_name}}</td>
                                                <td>@{{d.qty}}</td>
                                                <td>@{{d.price}}</td>
                                                <td>@{{d.subtotal}}</td>
                                            <td><a href="javascript:void(0)" v-bind:subtotal="d.subtotal" v-bind:data="d.id" @click="showAttribute(d.subtotal,d.id)" class="btn bg-teal btn waves-effect show-attribute"><i class="material-icons">add</i><span>Action</span></a></td>
                                            </tr>
                                            
                                             
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                              
                        </div>
                    </div>
                </div>
            </div>

            <!-- #END# Vertical Layout -->
           
        </div>
@endsection

@section('extra-script')
	{{Html::script('bsbmd/plugins/autosize/autosize.js')}}
	{{Html::script('bsbmd/plugins/momentjs/moment.js')}}
	{{Html::script('bsbmd/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}
	{{Html::script('bsbmd/js/pages/forms/basic-form-elements.js')}}
    {{Html::script('bsbmd/plugins/jquery-validation/jquery.validate.js')}}
    {{Html::script('bsbmd/plugins/jquery-steps/jquery.steps.js')}}
    {{Html::script('bsbmd/plugins/sweetalert/sweetalert.min.js')}}
    {{Html::script('bsbmd/js/pages/forms/form-validation.js')}}
    {{Html::script('vue/dist/vue.min.js')}}
    {{Html::script('vue/dist/vue.js')}}
    {{Html::script('axios/axios/dist/axios.min.js')}}
    {{Html::script('axios/axios/dist/axios.js')}}

    <script>
        $('.data-destroy').on('click', function(){
            let id = $(this).data('id');
            $('#destroy-'+id).submit();
        });
    </script>
    <script>
        var link = "{{route('transaksi.index.api')}}";
        var ds = [];
        new Vue({
            el: '#learnvue',
            data:{
                pesan: 'learnvue',
                pesan2:'vuejs',
                pesan3: '<b>vue html</b>',
                bool1:true,
                bool2:'false',
                resultbooltrue:'True!!',
                resultboolfalse:'False!!',
                ds:[],
                index:0,
                filter: true,
                seen:true,
            },
            watch:{
               
            },
            methods:{
                indexCounter:function(a){
                        ++a;
                     this.index=a;
                     console.log(this.index);
                     return this.index;
                },
                showHideMethod:function(){
                   // $('.show-hide').hide();
                },
                showAttribute:function(subtotal,id){
                    console.log(subtotal,id);
                    //console.log(this.$attrs['subtotal']);

                },
                indexes: function(index) {
                    return index+1;
                }
                
            },
            computed:{
                indexCounter:function(){
                     return ++this.index;   
                }
            },
            mounted() {
                //axios.get(link).then(response => console.log(response.data));
             axios.get(link).then(response => this.ds=response.data
                
             );
            },
        });
        
    </script>
@endsection