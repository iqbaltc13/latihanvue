<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;

class UserNotAdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $length=rand(1,20);
        for ($i=0; $i <$length; $i++) { 
            $user = User::create([
                'name' =>$faker->name ,
                'email' => $faker->email,
                'status' => 'Active',
                'password' => bcrypt('secret')
            ]);
            $user->assignRole('user');
           
        }
        
    }
}
