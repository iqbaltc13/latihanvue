<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Store;
use Faker\Factory as Faker;


class ProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $length=rand(1,30);
        $store=Store::select('id','name')->get();
        for ($i=0; $i<$length ; $i++) { 
            $randStore=rand(0,sizeof($store)-1);
            $price=rand(10,100);
            $product=Product::create([
                'store_id'=>$store[$randStore]->id,
                'price'=>$price *10000,
                'name'=>$store[$randStore]->name.rand(1,50),
            ]);
        }

        //
    }
}
