<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Sales;
use App\Product;
use App\SalesDetail;

class SalesDetailSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sales=Sales::select('id')->get();
        $product=Product::select('id')->get();
        $faker = Faker::create();
        $length=rand(1,50);
        for ($i=0; $i<$length ; $i++) { 
            $salesDetail=SalesDetail::create([
                'product_id'=>$product[rand(0,sizeof($product)-1)]->id,
                'sales_id'=>$sales[rand(0,sizeof($sales)-1)]->id,
                'qty'=>rand(10,50),
            ]);
        }
    }
}
