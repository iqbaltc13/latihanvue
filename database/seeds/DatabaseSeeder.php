<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //   $this->call(PermissionSeed::class);
        //   $this->call(RoleSeed::class);
        //   $this->call(UserSeed::class);
        $this->call(UserNotAdminSeed::class);
        $this->call(SalesSeed::class);
        $this->call(StoreSeed::class);
        $this->call(ProductSeed::class);
        $this->call(SalesDetailSeed::class);
        
    }
}
