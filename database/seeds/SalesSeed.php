<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Sales;
use Faker\Factory as Faker;

class SalesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $length=rand(1,20);
        $user=User::select('id')->where('id','!=',1)->get();

        for ($i=0; $i<$length; $i++) { 
            $sales=Sales::create([
                'user_id'=>$user[rand(0,sizeof($user)-1)]->id,
                'sale_date'=>$faker->date($format = 'Y-m-d', $max = 'now'), 

            ]);
        }
    }
}
