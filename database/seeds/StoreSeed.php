<?php

use Illuminate\Database\Seeder;
use App\Store;
use App\User;
use Faker\Factory as Faker;

class StoreSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $length=rand(1,20);
        $user=User::select('id')->where('id','!=',1)->get();
        for ($i=0; $i<$length ; $i++) { 
            $store=Store::create([
                'user_id'=>$user[rand(0,sizeof($user)-1)]->id,
                'name'=>$faker->name,
                'address'=>$faker->address, 
            ]);
        }
    }
}
