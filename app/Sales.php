<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table ='sales' ;
    public $timestamps = true;
    protected $guarded=[];
    protected $appends=['user_name'];
    public function getUserNameAttribute(){
      return $this->user->name;
    }
    
    public function user(){
		
		return $this->belongsTo('App\User', 'user_id');
	
    }
    public function salesDetail()
    {
      
		
        return $this->hasMany('App\SalesDetail', 'sales_id','id');
        
        
    }
}
