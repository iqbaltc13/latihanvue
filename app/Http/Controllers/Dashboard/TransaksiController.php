<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Sales;
use App\SalesDetail;
use App\Store;
use Auth;
use StdCass;
use App\ProductWithCast;

class TransaksiController extends Controller
{
    public function index(){
   
       $model= SalesDetail::all();
       
       return response()->json($model);
    }
    public function indexView(){
        return view('transaksi.index');
    }
    public function allProduct(){
        $model= ProductWithCast::select('id')->get();
       
        return response()->json($model);
    }
}
