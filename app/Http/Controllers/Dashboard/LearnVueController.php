<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LearnVueController extends Controller
{
    protected $view='learn_vue.';
    public function index(){
        return view($this->view.'index');
    }
}
