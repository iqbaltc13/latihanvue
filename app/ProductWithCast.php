<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductWithCast extends Model
{
    protected $table ='product' ;
    public $timestamps = true;
    protected $guarded=[];
    protected $appends=['seller','sales_detail_id'];
    protected $cast=[
        'sales_detail_id'=>'array',
    ];
    public function store(){
		return $this->belongsTo('App\Store','store_id');
    }
    public function salesDetail()
    {		
        return $this->hasMany('App\SalesDetail', 'product_id','id');
    }
    public function getSellerAttribute(){
        $userName= $this->store ? $this->store->user_name : '';
        return $userName;
    }
    public function getSalesDetailIdAttribute(){
        $salesDetail=$this->salesDetail;
        $id=[];
        for ($i=0; $i <sizeof($salesDetail) ; $i++) { 
            array_push($id,$salesDetail[$i]->id);
        }
        return $id;
    }
}
