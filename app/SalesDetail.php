<?php

namespace App;
use stdClass;

use Illuminate\Database\Eloquent\Model;

class SalesDetail extends Model
{
    protected $table ='sales_detail' ;
    public $timestamps = true;
    protected $guarded=[];
    protected $appends=['subtotal','price','product_name','tanggal_beli','penjual','pembeli'];
    public function salesDetail(){
      return $this->hasOne('App\SalesDetail', 'id');
    }
    
    public function sales(){
		
		return $this->belongsTo('App\Sales', 'sales_id');
	
    }
    public function product(){
		
		return $this->belongsTo('App\Product', 'product_id');
	
    }
    public function getTanggalBeliAttribute(){
      return $this->sales->sale_date;
    }
    public function getProductNameAttribute(){
      return $this->product->name;
    }
    public function getPriceAttribute(){
      return $this->product->price;
    }
    public function  getSubtotalAttribute(){

      
      $subtotal=$this->qty * $this->price;
     
      return $subtotal;
      

    }
    public function getPembeliAttribute(){
      return $this->sales->user_name;
    }
    public function getPenjualAttribute(){
      return $this->product->seller;
    }
}
