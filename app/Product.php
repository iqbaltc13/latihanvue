<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table ='product' ;
    public $timestamps = true;
    protected $guarded=[];
    protected $appends=['seller'];
    protected $cast=[
        'sales_detail_id'=>'array',
    ];
    public function store(){
		return $this->belongsTo('App\Store','store_id');
    }
    public function salesDetail()
    {		
        return $this->hasMany('App\SalesDetail', 'product_id','id');
    }
    public function getSellerAttribute(){
        return $this->store->user_name;
    }
   
}
