<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table ='store' ;
    public $timestamps = true;
    protected $guarded=[];
    protected $appends=['user_name'];
    public function product(){
		  return $this->hasMany('App\Product', 'store_id','id');
    }
    public function user(){
		  return $this->belongsTo('App\User', 'user_id');
    }
    public function getUserNameAttribute(){
      return $this->user->name;
    }
}
